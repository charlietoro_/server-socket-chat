package serversocketchat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author charlie
 */

public class DBConnection {
    
    private String DB = "socketChatService";
    private String PG_USER = "postgres";
    private String PG_PASSWORD = "P@stgresQ1";
    private String SERVER_HOST = "192.168.1.108";
    private String PG_PORT = "2345";
    
    private Connection connection = null;
    private Statement statement = null;
    
    public DBConnection() {
        try {
            this.connection = DriverManager.getConnection("jdbc:postgresql://"+SERVER_HOST+":"+PG_PORT+"/"+DB, PG_USER, PG_PASSWORD);
            System.out.println("Connected to PostgreSQL...");
            this.statement = this.connection.createStatement();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error in databse conneciton...\n" + e.getMessage());
        }
    }
    
    public String getLastname(String name) {
        try {
            String lastname = null;
            ResultSet response = this.statement.executeQuery("SELECT lastname FROM users WHERE name LIKE '%"+ name +"%'");
            while (response.next()) {
                lastname = response.getString("lastname");
            }
            return lastname;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error in request...\n" + e.getMessage());
            return null;
        }
    }
}
